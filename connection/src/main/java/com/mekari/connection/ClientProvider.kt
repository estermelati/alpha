package com.mekari.connection

import okhttp3.Authenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

object ClientProvider {

    fun builder(
        interceptors: Array<Interceptor>,
        authenticator: Authenticator? = null
    ): OkHttpClient.Builder {
        val builder = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
        for (i in interceptors) {
            builder.addInterceptor(i)
        }
        authenticator?.run(builder::authenticator)
        return builder
    }

}