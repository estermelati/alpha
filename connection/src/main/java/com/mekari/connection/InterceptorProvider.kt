package com.mekari.connection

import okhttp3.logging.HttpLoggingInterceptor

object InterceptorProvider {
    fun logging(enableLogging: Boolean): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply {
            level = if (enableLogging) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        }
}