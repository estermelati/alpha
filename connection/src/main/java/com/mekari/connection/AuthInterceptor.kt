package com.mekari.connection

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor() : Interceptor {

    companion object {
        const val RAPID_API_KEY = "34a877f7d0mshbb249b5cfb3df9fp130d42jsn6fc3a0d06589"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder = original.newBuilder()
        builder
            .addHeader("X-RapidAPI-Host", "hotels4.p.rapidapi.com")
            .addHeader("X-RapidAPI-Key", RAPID_API_KEY)
        return chain.proceed(builder.build())
    }
}