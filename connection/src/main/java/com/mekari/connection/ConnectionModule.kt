package com.mekari.connection

import com.mekari.common.Logging
import com.mekari.common.Proxy
import com.mekari.common.scope.AppScope
import dagger.Module
import dagger.Provides
import okhttp3.Authenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
class ConnectionModule {

    @Provides
    @Singleton
    @Proxy
    @AppScope
    fun provideAuthInterceptor(): Interceptor = AuthInterceptor()

    @Provides
    @Singleton
    @Logging
    @AppScope
    fun provideHttpLoggingInterceptor(): Interceptor =
        InterceptorProvider.logging(true)

    @Provides
    @Singleton
    @AppScope
    fun provideOkHttpClientBuilder(
        @Proxy interceptorProxy: Interceptor,
        @Proxy authenticatorProxy: Authenticator,
        @Logging loggingInterceptor: Interceptor
    ): OkHttpClient =
        ClientProvider.builder(
            arrayOf(interceptorProxy, loggingInterceptor),
            authenticatorProxy
        ).build()

    @Provides
    @Singleton
    @AppScope
    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return RetrofitProvider.builder("https://hotels4.p.rapidapi.com", client)
    }
}