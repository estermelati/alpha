package com.mekari.alpha.presentation

import android.content.Intent
import android.os.Bundle
import com.mekari.alpha.R
import com.mekari.common.extension.toast
import com.mekari.property.presentation.PropertyActivity
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AndroidInjection.inject(this)

        btnProperty.setOnClickListener { startActivity(Intent(this, PropertyActivity::class.java)) }
    }

    override fun onResume() {
        super.onResume()
        toast("Hi again~")
    }
}
