package com.mekari.alpha

import android.content.Context
import androidx.multidex.MultiDex
import com.mekari.alpha.di.DaggerAlphaComponent
import com.mekari.base.di.BaseComponent
//import com.mekari.base.di.BaseComponentProvider
//import com.mekari.base.di.DaggerBaseComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

class AlphaApplication : DaggerApplication()
//    , BaseComponentProvider
{

    private lateinit var baseComponent: BaseComponent

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAlphaComponent
            .builder()
            .application(this)
//            .baseComponent(provideBaseComponent())
            .build()
    }

//    override fun provideBaseComponent(): BaseComponent {
//        if (!this::baseComponent.isInitialized) {
//            baseComponent = DaggerBaseComponent
//                .builder()
//                .build()
//        }
//        return baseComponent
//    }
}