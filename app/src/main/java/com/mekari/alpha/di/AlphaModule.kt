package com.mekari.alpha.di

import android.content.Context
import com.mekari.alpha.AlphaApplication
import dagger.Module
import dagger.Provides

@Module
class AlphaModule {

    @Provides
    fun AlphaApplication.bindApplicationContext(): Context = this
}