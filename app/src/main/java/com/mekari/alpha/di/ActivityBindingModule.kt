package com.mekari.alpha.di

import com.mekari.alpha.presentation.MainActivity
import com.mekari.common.scope.AppScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBindingModule {

    @AppScope
    @ContributesAndroidInjector
    fun mainActivity(): MainActivity
}