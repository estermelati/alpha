package com.mekari.alpha.di

import android.app.Application
import com.mekari.alpha.AlphaApplication
import com.mekari.base.di.BaseComponent
import com.mekari.common.scope.AppScope
import com.mekari.connection.ConnectionModule
import com.mekari.domain.di.DomainModule
import com.mekari.property.di.FeaturePropertyBindingModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@AppScope
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityBindingModule::class,
        AndroidSupportInjectionModule::class,
        AlphaModule::class,
        ConnectionModule::class,
        DomainModule::class,

        //feature-property
        FeaturePropertyBindingModule::class
    ]
//    , dependencies = [BaseComponent::class]
)
interface AlphaComponent : AndroidInjector<AlphaApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

//        fun baseComponent(baseComponent: BaseComponent): Builder

        fun build(): AlphaComponent
    }
}