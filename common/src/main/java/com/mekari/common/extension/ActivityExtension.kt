package com.mekari.common.extension

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat

private fun isPermissionNeeded(context: Context, permission: String) =
    ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED

fun Activity.hasPermissions(permissions: Array<String>) =
    !permissions.map { isPermissionNeeded(this, it) }.contains(true)

fun Activity.isRequestPermissionRationaleNeeded(permission: String) =
    if (isPermissionNeeded(this, permission)) {
        ActivityCompat.shouldShowRequestPermissionRationale(this, permission)
    } else {
        false
    }

fun Activity.requestPermission(permissions: Array<String>, requestCode: Int) {
    ActivityCompat.requestPermissions(
        this, permissions,
        requestCode
    )
}