package com.mekari.common.extension

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide

fun View.onLoading(state: Boolean) {
    visibility = if (state) View.VISIBLE else View.GONE
}

fun ImageView.loadUrl(url: String) {
    Glide.with(context)
        .load(url)
        .into(this)
}