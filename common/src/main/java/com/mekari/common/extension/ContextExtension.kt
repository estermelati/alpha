package com.mekari.common.extension

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.showAlertDialog(
    @StringRes message: Int,
    onPositiveClick: (() -> Unit)? = null,
    onNegativeClick: (() -> Unit)? = null
) {
    AlertDialog.Builder(this@showAlertDialog)
        .apply {
            setMessage(message)
            setCancelable(false)
            onPositiveClick?.run {
                setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                    onPositiveClick()
                }
            }
            onNegativeClick?.run {
                setNegativeButton(android.R.string.cancel) { dialog, _ ->
                    dialog.dismiss()
                    onNegativeClick()
                }
            }
        }
        .create()
        .show()
}