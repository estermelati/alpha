package com.mekari.data.api.dto

import com.google.gson.annotations.SerializedName


class PropertyDto {

    @SerializedName("result")
    lateinit var result: String
    @SerializedName("data")
    lateinit var data: DataDto

    class DataDto(
        @SerializedName("header") var header: String,
        @SerializedName("query") var query: QueryDto,
        @SerializedName("searchResults") var searchResults: SearchResultDto
    )

    class QueryDto(
        @SerializedName("destination") var destination: DestinationDto
    )

    class DestinationDto(
        @SerializedName("id") var id: String,
        @SerializedName("value") var value: String,
        @SerializedName("resolvedLocation") var resolvedLocation: String
    )

    class SearchResultDto(
        @SerializedName("totalCount") var totalCount: Int,
        @SerializedName("results") var results: List<ResultDto>
    )

    class ResultDto(
        @SerializedName("id") var id: String,
        @SerializedName("name") var name: String,
        @SerializedName("thumbnailUrl") var thumbnailUrl: String,
        @SerializedName("starRating") var starRating: Int,
        @SerializedName("address") var address: AddressDto,
        @SerializedName("welcomeRewards") var welcomeRewards: WelcomeRewardsDto,
        @SerializedName("guestReviews") var guestReviews: GuestReviewsDto,
        @SerializedName("tripAdvisorGuestReviews") var tripAdvisorGuestReviews: TripAdvisorGuestReviewsDto,
        @SerializedName("ratePlan") var ratePlan: RatePlanDto,
        @SerializedName("neighbourhood") var neighbourhood: String,
        @SerializedName("deals") var deals: DealsDto,
        @SerializedName("messaging") var messaging: MessagingDto,
        @SerializedName("pimmsAttributes") var pimmsAttributes: String,
        @SerializedName("coupon") var coupon: String,
        @SerializedName("coordinate") var coordinate: CoordinateDto,
        @SerializedName("roomsLeft") var roomsLeft: Int,
        @SerializedName("providerType") var providerType: String,
        @SerializedName("supplierHotelId") var supplierHotelId: Double
    )

    class CoordinateDto(
        @SerializedName("lat") var lat: Double,
        @SerializedName("lon") var lon: Double
    )

    class MessagingDto(
        @SerializedName("scarcity") var scarcity: String
    )

    data class DealsDto(
        @SerializedName("greatRate") var greatRate: Boolean,
        @SerializedName("priceReasoning") var priceReasoning: String
    )

    class RatePlanDto(
        @SerializedName("price") var price: PriceDto,
        @SerializedName("features") var features: FeaturesDto,
        @SerializedName("type") var type: String
    )

    class FeaturesDto(
        @SerializedName("freeCancellation") var freeCancellation: Boolean,
        @SerializedName("paymentPreference") var paymentPreference: Boolean,
        @SerializedName("noCCRequired") var noCCRequired: Boolean
    )

    class PriceDto(
        @SerializedName("current") var current: String,
        @SerializedName("exactCurrent") var exactCurrent: Float,
        @SerializedName("old") var old: String,
        @SerializedName("info") var info: String,
        @SerializedName("additionalInfo") var additionalInfo: String,
        @SerializedName("totalPricePerStay") var totalPricePerStay: String
    )

    class TripAdvisorGuestReviewsDto(
        @SerializedName("rating") var rating: Float,
        @SerializedName("total") var total: Int
    )

    class GuestReviewsDto(
        @SerializedName("unformattedRating") var unformattedRating: Float,
        @SerializedName("rating") var rating: Float,
        @SerializedName("total") var total: Int,
        @SerializedName("scale") var scale: Int
    )

    class WelcomeRewardsDto(
        @SerializedName("collect") var collect: Boolean
    )

    class AddressDto(
        @SerializedName("streetAddress") var streetAddress: String,
        @SerializedName("extendedAddress") var extendedAddress: String,
        @SerializedName("locality") var locality: String,
        @SerializedName("postalCode") var postalCode: String,
        @SerializedName("region") var region: String,
        @SerializedName("countryName") var countryName: String,
        @SerializedName("countryCode") var countryCode: String
    )
}
