package com.mekari.data.api.mapper

import com.mekari.data.api.dto.PropertyDto
import com.mekari.data.common.Mapper
import com.mekari.domain.entity.*

class PropertyDtoMapper : Mapper<PropertyDto, PropertyData>() {

    override fun apply(from: PropertyDto): PropertyData? {
        return PropertyData(
            result = from.result,
            data = from.data.map()
        )
    }

    private fun PropertyDto.DataDto.map(): PropData {
        return PropData(this.header, this.query.map(), this.searchResults.map())
    }

    private fun PropertyDto.SearchResultDto.map(): SearchResultData {
        return SearchResultData(this.totalCount, this.results.map())
    }

    private fun List<PropertyDto.ResultDto>.map(): List<ResultData> {
        val results = mutableListOf<ResultData>()
        for (t in this) {
            results.add(
                ResultData(
                    t.id,
                    t.name,
                    t.thumbnailUrl,
                    t.starRating,
                    t.address.map(),
                    t.welcomeRewards.map(),
                    t.guestReviews.map(),
                    t.tripAdvisorGuestReviews.map(),
                    t.ratePlan.map(),
                    t.neighbourhood,
                    t.deals.map(),
                    t.messaging.map(),
                    t.pimmsAttributes,
                    t.coupon,
                    t.coordinate.map(),
                    t.roomsLeft,
                    t.providerType,
                    t.supplierHotelId
                )
            )
        }
        return results
    }

    private fun PropertyDto.CoordinateDto.map() = CoordinateData(this.lat, this.lon)

    private fun PropertyDto.MessagingDto.map() = MessagingData(this.scarcity)

    private fun PropertyDto.DealsDto.map() = DealsData(this.greatRate, this.priceReasoning)

    private fun PropertyDto.PriceDto.map() = PriceData(
        this.current,
        this.exactCurrent,
        this.old,
        this.info,
        this.additionalInfo,
        this.totalPricePerStay
    )

    private fun PropertyDto.FeaturesDto.map() =
        FeaturesData(this.freeCancellation, this.paymentPreference, this.noCCRequired)

    private fun PropertyDto.RatePlanDto.map() =
        RatePlanData(this.price.map(), this.features.map(), this.type)

    private fun PropertyDto.TripAdvisorGuestReviewsDto.map() =
        TripAdvisorGuestReviewsData(this.rating, this.total)

    private fun PropertyDto.GuestReviewsDto.map() =
        GuestReviewsData(this.unformattedRating, this.rating, this.total, this.scale)

    private fun PropertyDto.WelcomeRewardsDto.map() = WelcomeRewardsData(this.collect)

    private fun PropertyDto.AddressDto.map() = AddressData(
        this.streetAddress,
        this.extendedAddress,
        this.locality,
        this.postalCode,
        this.region,
        this.countryName,
        this.countryCode
    )

    private fun PropertyDto.QueryDto.map() = QueryData(this.destination.map())

    private fun PropertyDto.DestinationDto.map() =
        DestinationData(this.id, this.value, this.resolvedLocation)

}