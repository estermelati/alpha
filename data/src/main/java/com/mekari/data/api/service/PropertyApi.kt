package com.mekari.data.api.service

import com.mekari.data.api.dto.PropertyDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PropertyApi {

    @GET("/properties/list")
    fun getPropertyList(
        @Query("currency") currency: String,
        @Query("locale") locale: String,
        @Query("sortOrder") sortOrder: String,
        @Query("destinationId") destinationId: Int,
        @Query("pageNumber") pageNumber: Int,
        @Query("checkIn") checkIn: String,
        @Query("checkOut") checkOut: String,
        @Query("pageSize") pageSize: Int,
        @Query("adults1") adults1: Int
    ): Single<PropertyDto>

}