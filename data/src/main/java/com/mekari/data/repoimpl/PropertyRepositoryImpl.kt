package com.mekari.data.repoimpl

import com.mekari.common.OpenForTesting
import com.mekari.data.api.dto.PropertyDto
import com.mekari.data.api.service.PropertyApi
import com.mekari.data.common.Mapper
import com.mekari.domain.entity.PropertyData
import com.mekari.domain.repository.PropertyRepository
import io.reactivex.Single
import javax.inject.Inject

@OpenForTesting
class PropertyRepositoryImpl : PropertyRepository {

    @Inject
    lateinit var propertyDtoMapper: Mapper<PropertyDto, PropertyData>

    @Inject
    lateinit var propertyApi: PropertyApi

    override fun getPropertyList(
        currency: String,
        locale: String,
        sortOrder: String,
        destinationId: Int,
        pageNumber: Int,
        checkIn: String,
        checkOut: String,
        pageSize: Int,
        adults1: Int
    ): Single<PropertyData> {
        return propertyApi.getPropertyList(
            currency,
            locale,
            sortOrder,
            destinationId,
            pageNumber,
            checkIn,
            checkOut,
            pageSize,
            adults1
        ).map(propertyDtoMapper)
    }
}