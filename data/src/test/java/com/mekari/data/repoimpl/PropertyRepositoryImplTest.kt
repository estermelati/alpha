package com.mekari.data.repoimpl

import com.mekari.data.api.dto.PropertyDto
import com.mekari.data.api.service.PropertyApi
import com.mekari.data.common.Mapper
import com.mekari.domain.entity.PropertyData
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PropertyRepositoryImplTest {

    //SUT
    private lateinit var propertyRepoImpl: PropertyRepositoryImpl

    @Mock
    lateinit var mockedPropertyMapper: Mapper<PropertyDto, PropertyData>

    @Mock
    lateinit var mockedPropertyApi: PropertyApi

    @Mock
    lateinit var mockedSinglePropertyDto: Single<PropertyDto>

    @Mock
    lateinit var mockedSinglePropertyDao: Single<PropertyData?>

    @Before
    fun setUp() {
        propertyRepoImpl = PropertyRepositoryImpl()
        propertyRepoImpl.propertyDtoMapper = mockedPropertyMapper
        propertyRepoImpl.propertyApi = mockedPropertyApi
    }

    @Test
    fun test_givenValidData__whenRepoGetPropertyList_thenApiShouldGetPropertyList() {
        //given
        val givenCurrency = "currency"
        val givenLocale = "EN"
        val givenSortOrder = "ASD"
        val givenDestinationId = 20
        val givenPageNumber = 1
        val givenCheckIn = "2 PM"
        val givenCheckOut = "12 PM"
        val givenPageSize = 2
        val givenAdults = 2
        given(
            mockedPropertyApi.getPropertyList(
                givenCurrency,
                givenLocale,
                givenSortOrder,
                givenDestinationId,
                givenPageNumber,
                givenCheckIn,
                givenCheckOut,
                givenPageSize,
                givenAdults
            )
        ).willReturn(mockedSinglePropertyDto)

        given(
            mockedPropertyApi.getPropertyList(
                givenCurrency,
                givenLocale,
                givenSortOrder,
                givenDestinationId,
                givenPageNumber,
                givenCheckIn,
                givenCheckOut,
                givenPageSize,
                givenAdults
            ).map(mockedPropertyMapper)
        ).willReturn(mockedSinglePropertyDao)

        //when
        `when`(
            mockedPropertyApi.getPropertyList(
                givenCurrency,
                givenLocale,
                givenSortOrder,
                givenDestinationId,
                givenPageNumber,
                givenCheckIn,
                givenCheckOut,
                givenPageSize,
                givenAdults
            )
        ).thenReturn(mockedSinglePropertyDto)

        //when
        propertyRepoImpl.getPropertyList(
            givenCurrency,
            givenLocale,
            givenSortOrder,
            givenDestinationId,
            givenPageNumber,
            givenCheckIn,
            givenCheckOut,
            givenPageSize,
            givenAdults
        )

        //then
        then(mockedPropertyApi).should().getPropertyList(
            givenCurrency,
            givenLocale,
            givenSortOrder,
            givenDestinationId,
            givenPageNumber,
            givenCheckIn,
            givenCheckOut,
            givenPageSize,
            givenAdults
        )
        then(mockedPropertyApi).shouldHaveNoMoreInteractions()
        then(mockedSinglePropertyDto).shouldHaveNoMoreInteractions()
        then(mockedSinglePropertyDao).shouldHaveNoMoreInteractions()
        then(mockedPropertyMapper).shouldHaveNoMoreInteractions()
    }
}