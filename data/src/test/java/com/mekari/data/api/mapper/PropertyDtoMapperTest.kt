package com.mekari.data.api.mapper

import com.mekari.data.api.dto.PropertyDto
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PropertyDtoMapperTest {

    //SUT
    private lateinit var propertyDtoMapper: PropertyDtoMapper

    private lateinit var propertyDto: PropertyDto

    @Before
    fun setUp() {
        propertyDtoMapper = PropertyDtoMapper()
        propertyDto = fakePropertyDto()
    }

    @Test
    fun test_givenPropertyDto_whenMapperApply_thenShouldReturnPropertyData() {
        //when
        val mappedPropertyDto = propertyDtoMapper.apply(propertyDto)

        //assert
        assertThat(mappedPropertyDto?.result, `is`("result"))
        assertThat(mappedPropertyDto?.data?.header, `is`("header"))
        assertThat(mappedPropertyDto?.data?.query?.destination?.id, `is`("111"))
        assertThat(mappedPropertyDto?.data?.query?.destination?.value, `is`("destination value"))
        assertThat(mappedPropertyDto?.data?.query?.destination?.resolvedLocation, `is`("location"))
        assertThat(mappedPropertyDto?.data?.searchResults?.results!![0].roomsLeft, `is`(2))
    }

    private fun fakePropertyDto(): PropertyDto {
        val fakePropertyDto = PropertyDto()
        fakePropertyDto.result = "result"
        fakePropertyDto.data = fakeDataDto()

        return fakePropertyDto
    }

    private fun fakeDataDto(): PropertyDto.DataDto = PropertyDto.DataDto(
        "header",
        fakeQueryDto(),
        fakeSearchResultDto()
    )

    private fun fakeSearchResultDto(): PropertyDto.SearchResultDto = PropertyDto.SearchResultDto(
        10,
        resultsDto()
    )

    private fun resultsDto(): List<PropertyDto.ResultDto> = listOf(fakeResultDto())

    private fun fakeResultDto(): PropertyDto.ResultDto = PropertyDto.ResultDto(
        "1",
        "name",
        "thumbnail url",
        10,
        fakeAddressDto(),
        fakeWelcomeRewardsDto(),
        fakeGuestReviewDto(),
        fakeTripAdvisorGuestReviewDto(),
        fakeRatePlanDto(),
        "good",
        fakeDealsDto(),
        fakeMessagingDto(),
        "fake primms attribute",
        "C0UP0N",
        fakeCoordinateDto(),
        2,
        "provider type 1",
        123.0
    )

    private fun fakeMessagingDto(): PropertyDto.MessagingDto = PropertyDto.MessagingDto(
        "scarcity here"
    )

    private fun fakeDealsDto(): PropertyDto.DealsDto = PropertyDto.DealsDto(
        true,
        "price reasoning"
    )

    private fun fakeRatePlanDto(): PropertyDto.RatePlanDto = PropertyDto.RatePlanDto(
        fakePriceDto(),
        fakeFeatureDto(),
        "plan type 1"
    )

    private fun fakeFeatureDto(): PropertyDto.FeaturesDto = PropertyDto.FeaturesDto(
        freeCancellation = true,
        paymentPreference = true,
        noCCRequired = true
    )

    private fun fakePriceDto(): PropertyDto.PriceDto = PropertyDto.PriceDto(
        "Rp 1.000.000",
        1000000F,
        "Rp 1.500.000",
        "info here",
        "additional info here",
        "Rp 1.000.000"
    )

    private fun fakeTripAdvisorGuestReviewDto(): PropertyDto.TripAdvisorGuestReviewsDto =
        PropertyDto.TripAdvisorGuestReviewsDto(
            10F,
            20
        )

    private fun fakeCoordinateDto(): PropertyDto.CoordinateDto = PropertyDto.CoordinateDto(
        -6.1234,
        100.99989
    )

    private fun fakeGuestReviewDto(): PropertyDto.GuestReviewsDto = PropertyDto.GuestReviewsDto(
        10F,
        10F,
        20,
        10
    )

    private fun fakeWelcomeRewardsDto(): PropertyDto.WelcomeRewardsDto =
        PropertyDto.WelcomeRewardsDto(
            true
        )

    private fun fakeAddressDto(): PropertyDto.AddressDto = PropertyDto.AddressDto(
        "St. Address",
        "extended address here",
        "locality",
        "14022",
        "region",
        "ID",
        "+62"
    )

    private fun fakeQueryDto(): PropertyDto.QueryDto = PropertyDto.QueryDto(fakeDestinationDto())

    private fun fakeDestinationDto(): PropertyDto.DestinationDto = PropertyDto.DestinationDto(
        "111",
        "destination value",
        "location"
    )
}