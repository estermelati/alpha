package com.mekari.base.observer

import com.mekari.base.MappingException
import com.mekari.base.error.RepositoryErrorBundle
import com.mekari.base.presenter.BaseDataPresenter
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SingleObserverTest {

    //SUT (let's say Data is String, can be any object anyway)
    private lateinit var singleObserver: SingleObserver<List<String>>

    @Mock
    lateinit var mockedPresenter: BaseDataPresenter<*, List<String>>

    @Before
    fun setUp() {
        singleObserver = SingleObserver(mockedPresenter)
    }

    @Ignore("still not match between expected and actual method execution")
    @Test
    fun test_givenMappingException_whenSingleObserveOnError_thenPresenterShouldOnFailed_withRepositoryErrorBundle() {
        //given
        val givenCause = Throwable("message")
        val givenException = MappingException("exception message", givenCause)
        val givenRepositoryErrorBundle = RepositoryErrorBundle(givenException.message)

        //when
        singleObserver.onError(givenException)

        //then
        then(mockedPresenter).should().onFailed(givenRepositoryErrorBundle)
        then(mockedPresenter).shouldHaveNoMoreInteractions()
    }

    @Test
    fun test_givenDataNotEmpty_whenSingleObserverOnSuccess_thenPresenterShouldOnDataLoaded() {
        //given
        val givenData = listOf("this line is a data", "this line is a data too!")

        //when
        singleObserver.onSuccess(givenData)

        //then
        then(mockedPresenter).should().onDataLoaded(givenData)
        then(mockedPresenter).shouldHaveNoMoreInteractions()
    }

    @Test
    fun test_givenDataEmpty_whenSingleObserverOnSuccess_thenPresenterShouldOnDataEmpty() {
        //given
        val givenData = emptyList<String>()

        //when
        singleObserver.onSuccess(givenData)

        //then
        then(mockedPresenter).should().onDataEmpty()
    }
}