package com.mekari.base.presenter

import com.mekari.common.OpenForTesting

@OpenForTesting
abstract class BaseDataPresenter<V, D> : DataPresenter<V, D> {

    fun onComplete() { // No-op by default
    }

    fun onDataEmpty() { // No-op by default
    }

    override fun resume() { // No-op by default
    }

    override fun pause() { // No-op by default
    }

    override fun destroy() { // No-op by default
    }
}