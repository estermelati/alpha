package com.mekari.base.presenter

import com.mekari.base.error.ErrorBundle

interface DataPresenter<V, D> : Presenter {
    fun attachView(view: V)
    fun onDataLoaded(d: D)
    fun onFailed(errorBundle: ErrorBundle?)
}