package com.mekari.base.error

import java.net.SocketTimeoutException
import java.net.UnknownHostException

class DefaultErrorBundle(override val exception: Exception?) :
    ErrorBundle {

    override val message: String
        get() = when (exception) {
            is UnknownHostException -> {
                UNKNOWN_HOST_ERROR_MSG
            }
            is SocketTimeoutException -> {
                SOCKET_TIMEOUT_ERROR_MSG
            }
            else -> {
                DEFAULT_ERROR_MSG
            }
        }

    companion object {

        const val DEFAULT_ERROR_MSG = "Unknown error"

        const val SOCKET_TIMEOUT_ERROR_MSG =
            "Maaf, batas waktu koneksi ke sistem " + "telah habis, mohon coba kembali"

        const val UNKNOWN_HOST_ERROR_MSG =
            "Maaf, tidak ada koneksi internet " + "mobile atau Wi-fi pada Handphone Anda"
    }
}