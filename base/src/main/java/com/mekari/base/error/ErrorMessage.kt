package com.mekari.base.error

enum class ErrorMessage(val httpCode: Int, val message: String) {
    BAD_REQUEST(400, "Bad Request"),
    FORBIDDEN(403, "Forbidden"),
    NOT_FOUND(404, "Sorry, our service is under maintenance"),
    METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
    UNPROCESSABLE_ENTITY(422, "Unprocessable Entity"),
    TOO_MANY_REQUEST(429, "Too Many Request"),
    INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
    SERVICE_UNAVAILABLE(503, "Service Unavailable"),
    GATEWAY_TIMEOUT(504, "Gateway Timeout")
}