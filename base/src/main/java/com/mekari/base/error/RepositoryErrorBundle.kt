package com.mekari.base.error

class RepositoryErrorBundle(
    override val message: String? = null,
    override val exception: Exception? = null
) : ErrorBundle