package com.mekari.base.error

import java.lang.Exception

interface ErrorBundle {
    val exception: Exception?
    val message: String?
}