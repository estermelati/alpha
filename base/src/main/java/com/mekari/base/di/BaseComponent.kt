package com.mekari.base.di

import dagger.Component

@Component(modules = [BaseModule::class])
interface BaseComponent