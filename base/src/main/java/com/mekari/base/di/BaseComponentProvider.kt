package com.mekari.base.di

interface BaseComponentProvider {
    fun provideBaseComponent(): BaseComponent
}