package com.mekari.base.observer

import io.reactivex.observers.DisposableObserver

abstract class DefaultObserver<Thread> : DisposableObserver<Thread>() {

    override fun onNext(t: Thread) {
        onSuccess(t)
    }

    override fun onComplete() {
        // no implement
    }

    abstract fun onSuccess(data: Thread)
}
