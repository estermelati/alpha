package com.mekari.base.observer

import com.mekari.base.MappingException
import com.mekari.base.error.DefaultErrorBundle
import com.mekari.base.error.ErrorMessage
import com.mekari.base.error.RepositoryErrorBundle
import com.mekari.base.presenter.BaseDataPresenter
import com.mekari.common.OpenForTesting
import io.reactivex.observers.DisposableSingleObserver
import retrofit2.HttpException

@OpenForTesting
class SingleObserver<Data>(
    private val presenter: BaseDataPresenter<*, Data>
) : DisposableSingleObserver<Data>() {

    override fun onError(throwable: Throwable) {
        if (throwable is Exception) {
            if (throwable is MappingException) {
                presenter.onFailed(RepositoryErrorBundle(throwable.message))
            } else {
                if (throwable is HttpException) {
                    when {
                        throwable.code() == 404 -> presenter.onFailed(
                            RepositoryErrorBundle(
                                ErrorMessage.NOT_FOUND.message
                            )
                        )
                        throwable.code() == 504 -> presenter.onFailed(
                            RepositoryErrorBundle(
                                ErrorMessage.GATEWAY_TIMEOUT.message
                            )
                        )
                        else -> presenter.onFailed(RepositoryErrorBundle(ErrorMessage.INTERNAL_SERVER_ERROR.message))
                    }
                } else {
                    presenter.onFailed(DefaultErrorBundle(throwable))
                }
            }
        } else {
            presenter.onFailed(DefaultErrorBundle(null))
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun onSuccess(data: Data) {
        if (data is Collection<*> && (data as Collection<*>).isEmpty()) {
            presenter.onDataEmpty()
        } else {
            (presenter as BaseDataPresenter<Any, Data>).onDataLoaded(data)
        }
    }
}