package com.mekari.base.observer

import com.mekari.base.MappingException
import com.mekari.base.error.DefaultErrorBundle
import com.mekari.base.error.ErrorMessage
import com.mekari.base.error.RepositoryErrorBundle
import com.mekari.base.presenter.BaseDataPresenter
import retrofit2.HttpException

class DataObserver<Data>(
    private val presenter: BaseDataPresenter<*, Data>
) : DefaultObserver<Data>() {

    override fun onError(throwable: Throwable) {
        if (throwable is Exception) {
            if (throwable is MappingException) {
                presenter.onFailed(RepositoryErrorBundle(throwable.message))
            } else {
                if (throwable is HttpException) {
                    when {
                        throwable.code() == 404 -> {
                            presenter.onFailed(RepositoryErrorBundle(ErrorMessage.NOT_FOUND.message))
                        }
                        throwable.code() == 504 -> {
                            presenter.onFailed(RepositoryErrorBundle(ErrorMessage.GATEWAY_TIMEOUT.message))
                        }
                        else -> {
                            presenter.onFailed(RepositoryErrorBundle(ErrorMessage.INTERNAL_SERVER_ERROR.message))
                        }
                    }
                } else {
                    presenter.onFailed(DefaultErrorBundle(throwable))
                }
            }
        } else {
            presenter.onFailed(DefaultErrorBundle(null))
        }
    }

    override fun onSuccess(data: Data) {
        if (data is Collection<*> && (data as Collection<*>).isEmpty()) {
            presenter.onDataEmpty()
        } else {
            presenter.onDataLoaded(data)
        }
    }
}