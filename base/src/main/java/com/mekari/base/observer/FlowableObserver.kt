package com.mekari.base.observer

import com.mekari.base.MappingException
import com.mekari.base.error.DefaultErrorBundle
import com.mekari.base.error.ErrorMessage
import com.mekari.base.error.RepositoryErrorBundle
import com.mekari.base.presenter.BaseDataPresenter
import io.reactivex.subscribers.DisposableSubscriber
import retrofit2.HttpException

class FlowableObserver<Data>(
    private val presenter: BaseDataPresenter<*, Data>
) : DisposableSubscriber<Data>() {

    override fun onComplete() {
        //no implemented
    }

    @Suppress("UNCHECKED_CAST")
    override fun onNext(t: Data) {
        if (t is Collection<*> && (t as Collection<*>).isEmpty()) {
            presenter.onDataEmpty()
        } else {
            (presenter as BaseDataPresenter<Any, Data>).onDataLoaded(t)
        }
    }

    override fun onError(throwable: Throwable) {
        if (throwable is Exception) {
            if (throwable is MappingException) {
                presenter.onFailed(RepositoryErrorBundle(throwable.message))
            } else {
                if (throwable is HttpException) {
                    when {
                        throwable.code() == 404 -> presenter.onFailed(
                            RepositoryErrorBundle(
                                ErrorMessage.NOT_FOUND.message
                            )
                        )
                        throwable.code() == 504 -> presenter.onFailed(
                            RepositoryErrorBundle(
                                ErrorMessage.GATEWAY_TIMEOUT.message
                            )
                        )
                        else -> presenter.onFailed(RepositoryErrorBundle(ErrorMessage.INTERNAL_SERVER_ERROR.message))
                    }
                } else {
                    presenter.onFailed(DefaultErrorBundle(throwable))
                }
            }
        } else {
            presenter.onFailed(DefaultErrorBundle(null))
        }
    }
}