package com.mekari.base.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.mekari.base.R
import kotlinx.android.synthetic.main.view_error.view.*

class ErrorView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defAttributeSet: Int = 0
) : FrameLayout(context, attributeSet, defAttributeSet) {

    private var listener: Listener? = null

    init {
        initView()
        btnRetry.setOnClickListener {
            listener?.onRetryClick()
        }
    }

    private fun initView() {
        LayoutInflater.from(context).inflate(R.layout.view_error, this)
    }

    fun setErrorImage(imageResId: Int) {
        if (imageResId == 0) {
            ivError.visibility = View.GONE
        } else {
            ivError.setImageResource(imageResId)
            ivError.visibility = View.VISIBLE
        }
        visibility = View.VISIBLE
    }

    fun setErrorMessage(errorMessage: String?) {
        if (errorMessage != null && errorMessage.isNotEmpty()) {
            tvErrorMessage.text = errorMessage
            tvErrorMessage.visibility = View.VISIBLE
        } else {
            tvErrorMessage.visibility = View.GONE
        }
        visibility = View.VISIBLE
    }

    fun setListener(listener: Listener) {
        this.listener = listener
    }

    fun getListener(): Listener? {
        return this.listener
    }

    fun showRetryButton(isShow: Boolean) {
        if (isShow) {
            btnRetry.visibility = View.VISIBLE
        } else {
            btnRetry.visibility = View.GONE
        }
    }

    fun getTextView(): String? {
        return tvErrorMessage.text.toString()
    }

    interface Listener {
        fun onRetryClick()
    }
}