package com.mekari.domain.di

import com.mekari.common.scope.AppScope
import com.mekari.domain.executor.AndroidUiThread
import com.mekari.domain.executor.JobExecutor
import com.mekari.domain.executor.PostExecutionThread
import com.mekari.domain.executor.ThreadExecutor
import dagger.Binds
import dagger.Module

@Module
interface DomainModule {
    @Binds
    @AppScope
    fun bindThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor

    @Binds
    @AppScope
    fun bindPostExecutionThread(androidUiThread: AndroidUiThread): PostExecutionThread

}