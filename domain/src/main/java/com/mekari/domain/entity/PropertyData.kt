package com.mekari.domain.entity

data class PropertyData(
    val result: String,
    val data: PropData
)

data class PropData(
    val header: String,
    val query: QueryData,
    val searchResults: SearchResultData
)

data class SearchResultData(
    val totalCount: Int,
    val results: List<ResultData>
)

data class ResultData(
    val id: String,
    val name: String,
    val thumbnailUrl: String,
    val starRating: Int,
    val address: AddressData,
    val welcomeRewards: WelcomeRewardsData,
    val guestReviews: GuestReviewsData,
    val tripAdvisorGuestReviews: TripAdvisorGuestReviewsData,
    val ratePlan: RatePlanData,
    val neighbourhood: String,
    val dealsData: DealsData,
    val messaging: MessagingData,
    val pimmsAttributes: String,
    val coupon: String,
    val coordinate: CoordinateData,
    val roomsLeft: Int,
    val providerType: String,
    val supplierHotelId: Double
)

data class CoordinateData(
    val lat: Double,
    val lon: Double
)

data class MessagingData(
    val scarcity: String
)

data class DealsData(
    val greatRate: Boolean,
    val priceReasoning: String
)

data class RatePlanData(
    val price: PriceData,
    val features: FeaturesData,
    val type: String
)

data class PriceData(
    val current: String,
    val exactCurrent: Float,
    val old: String,
    val info: String,
    val additionalInfo: String,
    val totalPricePerStay: String
)

data class FeaturesData(
    val freeCancellation: Boolean,
    val paymentPreference: Boolean,
    val noCCRequired: Boolean
)

data class TripAdvisorGuestReviewsData(
    val rating: Float,
    val total: Int
)

data class GuestReviewsData(
    val unformattedRating: Float,
    val rating: Float,
    val total: Int,
    val scale: Int
)

data class AddressData(
    val streetAddress: String,
    val extendedAddress: String,
    val locality: String,
    val postalCode: String,
    val region: String,
    val countryName: String,
    val countryCode: String
)

data class WelcomeRewardsData(
    val collect: Boolean
)

data class QueryData(
    val destination: DestinationData
)

data class DestinationData(
    val id: String,
    val value: String,
    val resolvedLocation: String
)
