package com.mekari.domain.entity

data class BaseData(
    val propertyId: String,
    val propertyName: String,
    val city: String,
    val country: String
)