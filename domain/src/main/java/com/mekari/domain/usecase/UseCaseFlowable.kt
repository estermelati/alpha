package com.mekari.domain.usecase

import com.mekari.common.OpenForTesting
import com.mekari.domain.executor.PostExecutionThread
import com.mekari.domain.executor.ThreadExecutor
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DisposableSubscriber

@OpenForTesting
abstract class UseCaseFlowable<T, in Params>(
    private val threadExecutor: ThreadExecutor,
    private val postExecutionThread: PostExecutionThread
) {
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    /**
     * Builds an [Flowable] which will be used when executing the current [UseCase].
     */
    abstract fun build(params: Params): Flowable<T>

    fun execute(subscriber: DisposableSubscriber<T>, params: Params) {
        val disposable = build(params)
            .subscribeOn(Schedulers.from(threadExecutor))
            .observeOn(postExecutionThread.scheduler())
            .subscribeWith(subscriber)
        addDisposable(disposable)
    }

    /**
     * Dispose from current [CompositeDisposable].
     */
    fun dispose() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    /**
     * Dispose from current [CompositeDisposable].
     */
    private fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }
}