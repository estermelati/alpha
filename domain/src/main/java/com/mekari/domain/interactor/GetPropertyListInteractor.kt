package com.mekari.domain.interactor

import androidx.annotation.VisibleForTesting
import com.mekari.common.OpenForTesting
import com.mekari.domain.executor.PostExecutionThread
import com.mekari.domain.executor.ThreadExecutor
import com.mekari.domain.usecase.UseCaseSingle
import com.mekari.domain.entity.PropertyData
import com.mekari.domain.repository.PropertyRepository
import io.reactivex.Single
import javax.inject.Inject

@OpenForTesting
class GetPropertyListInteractor @Inject constructor(
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread,
    var propertyRepository: PropertyRepository
) : UseCaseSingle<PropertyData, GetPropertyListInteractor.Params>(
    threadExecutor,
    postExecutionThread
) {

    class Params(
        val currency: String,
        val locale: String,
        val sortOrder: String,
        val destinationId: Int,
        val pageNumber: Int,
        val checkIn: String,
        val checkOut: String,
        val pageSize: Int,
        val adults1: Int
    )

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun execute(params: Params): Single<PropertyData> {
        return propertyRepository.getPropertyList(
            params.currency,
            params.locale,
            params.sortOrder,
            params.destinationId,
            params.pageNumber,
            params.checkIn,
            params.checkOut,
            params.pageSize,
            params.adults1
        )
    }
}