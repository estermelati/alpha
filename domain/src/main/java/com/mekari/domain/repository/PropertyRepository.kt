package com.mekari.domain.repository

import com.mekari.domain.entity.PropertyData
import io.reactivex.Single

interface PropertyRepository {

    fun getPropertyList(
        currency: String,
        locale: String,
        sortOrder: String,
        destinationId: Int,
        pageNumber: Int,
        checkIn: String,
        checkOut: String,
        pageSize: Int,
        adults1: Int
    ): Single<PropertyData>

}