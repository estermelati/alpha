package com.mekari.domain.executor

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

class AndroidUiThread : PostExecutionThread {
    override fun scheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}