package com.mekari.domain.executor

import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * This is where we define how many concurrent processes are going to be executed
 */
class JobExecutor @Inject constructor() :
    ThreadExecutor {
    private val threadPoolExecutor: ThreadPoolExecutor
    override fun execute(runnable: Runnable?) {
        threadPoolExecutor.execute(runnable)
    }

    private class JobThreadFactory : ThreadFactory {
        private var counter = 0
        override fun newThread(runnable: Runnable): Thread {
            return Thread(runnable, "android_" + counter++)
        }
    }

    init {
        threadPoolExecutor = ThreadPoolExecutor(
            3, 5, 10, TimeUnit.SECONDS,
            LinkedBlockingQueue(),
            JobThreadFactory()
        )
    }
}