package com.mekari.property.presentation.presenter

import com.mekari.base.error.DefaultErrorBundle
import com.mekari.domain.entity.*
import com.mekari.domain.interactor.GetPropertyListInteractor
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class PropertyListPresenterTest {

    //SUT
    private lateinit var presenter: PropertyListPresenter

    @Mock
    lateinit var mockedInteractor: GetPropertyListInteractor

    @Mock
    lateinit var mockedView: PropertyListContract.View

    private fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> uninitialized(): T = null as T

    @Before
    @Throws(Exception::class)
    fun setUp() {
        presenter = PropertyListPresenter(mockedInteractor)
        presenter.attachView(mockedView)
    }

    @Test
    fun test_givenParams_whenPresenterGetList_verifyInteractorCallExecuteOnlyOnce() {
        //given
        val givenCurrency = "USD"
        val givenLocale = "EN"
        val givenSortOrder = "ASC"
        val givenDestinationId = 123
        val givenPageNumber = 1
        val givenCheckIn = "check in"
        val givenCheckOut = "check out"
        val givenPageSize = 1
        val givenAdult1 = 2

        //when
        presenter.getPropertyList(
            givenCurrency,
            givenLocale,
            givenSortOrder,
            givenDestinationId,
            givenPageNumber,
            givenCheckIn,
            givenCheckOut,
            givenPageSize,
            givenAdult1
        )

        //verify
        Mockito.verify(mockedInteractor, Mockito.times(1))
            .execute(any(), any())
    }

    @Test
    fun test_givenPropertyData_whenPresenterOnDataLoaded_thenViewShouldOnDataLoaded() {
        //given
        val givenData = PropertyData("", propData())

        //when
        presenter.onDataLoaded(givenData)

        //then
        then(mockedView).should().onDataLoaded(givenData)
        then(mockedView).shouldHaveNoMoreInteractions()
    }

    private fun propData(): PropData {
        return PropData("header", queryData(), searchResultData())
    }

    private fun searchResultData(): SearchResultData {
        return SearchResultData(1, resultData())
    }

    private fun resultData(): List<ResultData> {
        val results = mutableListOf<ResultData>()
        results.add(
            ResultData(
                "1",
                "name hotel",
                "https://scontent.fcgk10-1.fna.fbcdn.net/v/t1.0-9/56721611_2479997258894654_863734312820801536_n.jpg?_nc_cat=102&_nc_sid=09cbfe&_nc_eui2=AeEbyyuDvVfNLgy2mt83wqTbWzX6LXaERudbNfotdoRG5y1GmglKH-5QV4I0Iq01rdaru8oCBbomB8fD_ASslz0C&_nc_ohc=5_jgDRiOS_AAX9-qE1d&_nc_ht=scontent.fcgk10-1.fna&oh=9da67f6611a934cdca18a6be6b37eb39&oe=5F0CF5B9",
                10,
                address(),
                welcomeRewards(),
                guestReviews(),
                tripAdvisorGuestReviews(),
                ratePlan(),
                "safe neighbourhood",
                deals(),
                messaging(),
                "-",
                "HOTEL20",
                coordinate(),
                1,
                "provider",
                20.0
            )
        )
        return results
    }

    private fun coordinate(): CoordinateData = CoordinateData(-6.200000, 106.816666)

    private fun messaging(): MessagingData = MessagingData("scarcity")

    private fun deals(): DealsData = DealsData(true, "price reasoning")

    private fun ratePlan(): RatePlanData = RatePlanData(price(), features(), "type")

    private fun features(): FeaturesData = FeaturesData(
        freeCancellation = true,
        paymentPreference = false,
        noCCRequired = true
    )

    private fun price(): PriceData = PriceData("$100", 100F, "$120", "info", "-", "$100")

    private fun tripAdvisorGuestReviews(): TripAdvisorGuestReviewsData =
        TripAdvisorGuestReviewsData(
            10F, 100
        )

    private fun guestReviews(): GuestReviewsData = GuestReviewsData(10F, 10F, 100, 10)

    private fun welcomeRewards(): WelcomeRewardsData = WelcomeRewardsData(true)

    private fun address(): AddressData = AddressData(
        "street address",
        "extended address",
        "EN",
        "CA",
        "region",
        "US",
        "country code"
    )

    private fun queryData(): QueryData = QueryData(destinationData())

    private fun destinationData(): DestinationData = DestinationData("id", "value", "resLocation")

    @Test
    fun test_givenError_whenPresenterOnFailed_thenViewShouldOnLoadingFailed() {
        //given
        val givenException = Exception("Sorry, this is only for testing exception message")
        val givenError = DefaultErrorBundle(givenException)

        //when
        presenter.onFailed(givenError)

        //then
        then(mockedView).should().onLoadingFailed(givenError.message)
        then(mockedView).shouldHaveNoMoreInteractions()
    }

    @Test
    fun test_whenPresenterDestroy_thenInteractorShouldDispose() {
        //when
        presenter.destroy()

        //then
        then(mockedInteractor).should().dispose()
        then(mockedInteractor).shouldHaveNoMoreInteractions()
    }
}