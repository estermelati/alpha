package com.mekari.property.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mekari.common.extension.toast
import com.mekari.domain.entity.PropertyData
import com.mekari.property.R
import com.mekari.property.presentation.presenter.PropertyListContract
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.fragment_property.*
import javax.inject.Inject

class PropertyFragment @Inject constructor(): Fragment(), PropertyListContract.View, HasSupportFragmentInjector {

    @Inject
    lateinit var propertyListPresenter: PropertyListContract.Presenter

    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        propertyListPresenter.attachView(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_property, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        propertyListPresenter.getPropertyList(
            "USD",
            "en_US",
            "PRICE",
            destinationId = 1506246,
            pageNumber = 1,
            checkIn = "2020-01-08",
            checkOut = "2020-01-15",
            pageSize = 25,
            adults1 = 1
        )
        tvLoading.visibility = View.VISIBLE
    }

    override fun onDataLoaded(d: PropertyData) {
        tvLoading.visibility = View.GONE
        activity?.toast("search result #1 = ${d.data.searchResults.results[0].name}")
    }

    override fun onLoadingFailed(message: String) {
        tvLoading.visibility = View.GONE
        activity?.toast(message)
    }

    override fun onDestroy() {
        super.onDestroy()
        propertyListPresenter.destroy()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return childFragmentInjector
    }

}