package com.mekari.property.presentation.presenter

import com.mekari.base.presenter.DataPresenter
import com.mekari.domain.entity.PropertyData

interface PropertyListContract {

    interface View {
        fun onDataLoaded(d: PropertyData)
        fun onLoadingFailed(message: String)
    }

    interface Presenter : DataPresenter<View, PropertyData> {
        fun getPropertyList(
            currency: String,
            locale: String,
            sortOrder: String,
            destinationId: Int,
            pageNumber: Int,
            checkIn: String,
            checkOut: String,
            pageSize: Int,
            adults1: Int
        )
    }

}