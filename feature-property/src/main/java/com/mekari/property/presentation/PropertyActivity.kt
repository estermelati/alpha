package com.mekari.property.presentation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.mekari.common.extension.toast
import com.mekari.domain.entity.PropertyData
import com.mekari.property.R
import com.mekari.property.presentation.presenter.PropertyListContract
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.fragment_property.*
import javax.inject.Inject

class PropertyActivity : DaggerAppCompatActivity(), PropertyListContract.View {

    @Inject
    lateinit var propertyListPresenter: PropertyListContract.Presenter

//    @Inject
//    internal lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onStart() {
        super.onStart()
        propertyListPresenter.attachView(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property)
        AndroidInjection.inject(this)
        toast("Welcome to PropertyActivity")
        propertyListPresenter.getPropertyList(
            "USD",
            "en_US",
            "PRICE",
            destinationId = 1506246,
            pageNumber = 1,
            checkIn = "2020-01-08",
            checkOut = "2020-01-15",
            pageSize = 25,
            adults1 = 1
        )
    }

//    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
////        return fragmentInjector
////    }

    override fun onDataLoaded(d: PropertyData) {
        toast("search result #1 = ${d.data.searchResults.results[0].name}")
    }

    override fun onLoadingFailed(message: String) {
        toast(message)
    }

    override fun onDestroy() {
        super.onDestroy()
        propertyListPresenter.destroy()
    }

}