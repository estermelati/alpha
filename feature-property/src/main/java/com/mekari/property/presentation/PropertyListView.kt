package com.mekari.property.presentation

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.mekari.common.extension.toast
import com.mekari.property.R

class PropertyListView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttributeSet: Int = 0
): FrameLayout(context, attributeSet, defStyleAttributeSet) {

    //is able to call api

    init {
        LayoutInflater.from(context).inflate(R.layout.view_list_property, this)
        val data = mutableListOf<String>()
        data.add("Data 1")
        data.add("Data 2")
        data.add("Data 3")
        showData(data)
    }

    private fun showData(data: List<String>) {
        context.toast("you must've called showData(), dataCount = ${data.size}")
    }
}