package com.mekari.property.presentation.presenter

import com.mekari.base.error.ErrorBundle
import com.mekari.base.observer.SingleObserver
import com.mekari.base.presenter.BaseDataPresenter
import com.mekari.domain.entity.PropertyData
import com.mekari.domain.interactor.GetPropertyListInteractor
import javax.inject.Inject

class PropertyListPresenter @Inject constructor(
    private val propertyListInteractor: GetPropertyListInteractor
) : BaseDataPresenter<PropertyListContract.View, PropertyData>(), PropertyListContract.Presenter {

    lateinit var view: PropertyListContract.View

    override fun getPropertyList(
        currency: String,
        locale: String,
        sortOrder: String,
        destinationId: Int,
        pageNumber: Int,
        checkIn: String,
        checkOut: String,
        pageSize: Int,
        adults1: Int
    ) {
        propertyListInteractor.execute(
            SingleObserver(this),
            GetPropertyListInteractor.Params(
                currency,
                locale,
                sortOrder,
                destinationId,
                pageNumber,
                checkIn,
                checkOut,
                pageSize,
                adults1
            )
        )
    }

    override fun attachView(view: PropertyListContract.View) {
        this.view = view
    }

    override fun onDataLoaded(d: PropertyData) {
        view.onDataLoaded(d)
    }

    override fun onFailed(errorBundle: ErrorBundle?) {
        view.onLoadingFailed(errorBundle?.message.orEmpty())
    }

    override fun destroy() {
        super.destroy()
        propertyListInteractor.dispose()
    }
}