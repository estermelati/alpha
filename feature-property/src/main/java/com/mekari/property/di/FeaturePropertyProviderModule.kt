package com.mekari.property.di

import com.mekari.data.api.mapper.PropertyDtoMapper
import com.mekari.data.api.service.PropertyApi
import com.mekari.data.repoimpl.PropertyRepositoryImpl
import com.mekari.domain.executor.PostExecutionThread
import com.mekari.domain.executor.ThreadExecutor
import com.mekari.domain.interactor.GetPropertyListInteractor
import com.mekari.domain.repository.PropertyRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class FeaturePropertyProviderModule {

    @Provides
    fun providePropertyApi(retrofit: Retrofit): PropertyApi =
        retrofit.create(PropertyApi::class.java)

    @Provides
    fun providePropertyRepository(propertyApi: PropertyApi): PropertyRepository {
        val propertyRepositoryImpl = PropertyRepositoryImpl()
        propertyRepositoryImpl.propertyApi = propertyApi
        propertyRepositoryImpl.propertyDtoMapper = PropertyDtoMapper()
        return propertyRepositoryImpl
    }

    @Provides
    fun provideGetPropertyListInteractor(
        threadExecutor: ThreadExecutor,
        postExecutionThread: PostExecutionThread,
        repository: PropertyRepository
    ) =
        GetPropertyListInteractor(threadExecutor, postExecutionThread, repository)

}