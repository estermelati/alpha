package com.mekari.property.di

import com.mekari.common.scope.AppScope
import com.mekari.property.presentation.PropertyActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FeaturePropertyBindingModule {

    @AppScope
    @ContributesAndroidInjector(modules = [FeaturePropertyModule::class, FeaturePropertyProviderModule::class])
    fun propertyActivity(): PropertyActivity
//
//    @ContributesAndroidInjector()
//    fun propertyFragment(): PropertyFragment
}