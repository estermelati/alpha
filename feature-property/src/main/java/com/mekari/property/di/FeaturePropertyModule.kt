package com.mekari.property.di

import com.mekari.common.scope.AppScope
import com.mekari.property.presentation.presenter.PropertyListContract
import com.mekari.property.presentation.presenter.PropertyListPresenter
import dagger.Binds
import dagger.Module

@Module
interface FeaturePropertyModule {

    @Binds
    fun bindPropertyListPresenter(propertyPresenter: PropertyListPresenter): PropertyListContract.Presenter

}