# README #

Please do not use branch master (it's unfinished and can't be run), just go to branch dagger-hilt & use that.

### What is this repository for? ###

Android Architecture Proposal for Talenta Attendance or any Android apps in Mekari that uses MVP architecture pattern

### Docs? ###

https://docs.google.com/document/d/1MusIqdG0eCv_xoXL9q9hwOZL7oFCmbhna9QP4WufMj8/edit?usp=sharing

### Who do I talk to? ###

Flock me @ Ester Melati
or any Android Developer guys you know @ Mekari